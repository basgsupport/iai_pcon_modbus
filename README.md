﻿---

## About

This is a sample for interfacing TwinCAT with IAI PCON controller through Modbus command.

---


## Pre-requisite

Make sure that the PCON IAI controller is configured correctly. The sample program will be moving the motor to position 0 and 5.

The baudrate used here is 38400, 8 data bits, 1 stop bit, no parity.


---

## How to use

When you run the code, it will do these things in sequence:

1. Turn IAI PIO on

2. Turn servo on

3. Homing

4. Move to position 5

5. Move to position 0


Turn variable `bContinuous` to TRUE to keep the movement continuous.


---

## Help

Contact support@beckhoff.com.sg

---

## Styling syntax

[Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

[Markdown preview](https://dillinger.io/)




